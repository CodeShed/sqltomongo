﻿############################################################################################
###
###  This script will create two docker containers in your local docker instance.
###    The first is an instance of SQL 2017 with the MS sample database WorldWideImporters.
###    The second is a container with MongoDb.
###
###  If you need to sign when running the script then, copy the contents of the file into
###    a new instace of powershell.  This will treat the script as a script that you just 
###    typed in and not as a file, negating the need to sign the powershel script file.
###
###########################################################################################

### https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-2017
cls

Write-Host "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
$scriptPath = $MyInvocation.MyCommand.Definition
Write-Host $scriptPath
Write-Host "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
##########################################################################################
###
$NameOfSqlServer = "SQL_Demo";
$SqlPassword     = "Testing123";
###
##########################################################################################


docker rm --force $NameOfSqlServer


##docker run --name mongodb bitnami/mongodb:latest

docker pull mcr.microsoft.com/mssql/server:2017-latest;
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=$SqlPassword" `
   -p 1433:1433 --name $NameOfSqlServer `
   -d mcr.microsoft.com/mssql/server:2017-latest



Write-Host "################################################"
##curl -OutFile "wwi.bak" "https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0/WideWorldImporters-Full.bak"

docker exec -it $NameOfSqlServer mkdir /var/opt/mssql/backup
docker cp wwi.bak ${NameOfSqlServer}:/var/opt/mssql/backup



docker exec -it $NameOfSqlServer /opt/mssql-tools/bin/sqlcmd -S localhost `
   -U SA -P "${SqlPassword}" `
   -Q "RESTORE FILELISTONLY FROM DISK = '/var/opt/mssql/backup/wwi.bak'"

docker exec -it $NameOfSqlServer /opt/mssql-tools/bin/sqlcmd `
   -S localhost -U SA -P "${SqlPassword}" `
   -Q "RESTORE DATABASE WideWorldImporters FROM DISK = '/var/opt/mssql/backup/wwi.bak' WITH MOVE 'WWI_Primary' TO '/var/opt/mssql/data/WideWorldImporters.mdf', MOVE 'WWI_UserData' TO '/var/opt/mssql/data/WideWorldImporters_userdata.ndf', MOVE 'WWI_Log' TO '/var/opt/mssql/data/WideWorldImporters.ldf', MOVE 'WWI_InMemory_Data_1' TO '/var/opt/mssql/data/WideWorldImporters_InMemory_Data_1'"

docker exec -it $NameOfSqlServer /opt/mssql-tools/bin/sqlcmd `
   -S localhost -U SA -P "${SqlPassword}" `
   -Q "SELECT Name FROM sys.Databases"

Write-Host "################################################"

Start-Sleep -m 600
docker exec -it $NameOfSqlServer /opt/mssql-tools/bin/sqlcmd `
   -S localhost -U SA -P "${SqlPassword}" `
   -Q "SELECT cast(@@SERVERNAME as nvarchar(15)) [ServerName],   
        cast(SERVERPROPERTY('ComputerNamePhysicalNetBIOS') as nvarchar(15)) [ComputerNamePhysicalNetBIOS],   
        cast(SERVERPROPERTY('MachineName') as nvarchar(15)) [MachineName],  
        Cast(SERVERPROPERTY('ServerName') as nvarchar(15))"



Write-Host "################################################"
Write-Host "#### SQL Restore completed              ########"
Write-Host "################################################"

#################################################################################################
docker rm --force mongodb
docker run -d -p 27017:27017 --name mongodb bitnami/mongodb:latest


Write-Host "################################################"
Write-Host "#### MongoDb Created                    ########"
Write-Host "################################################"

docker ps -a