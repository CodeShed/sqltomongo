﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using BCL.SqlToMongo.enums;
using Microsoft.Extensions.Configuration;

namespace BCL.SqlToMongo.Repository
{
    public class SQLRepository : IDisposable

    {
        public SQLRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private readonly IConfiguration _configuration;
        private SqlConnection _conn;

        public async Task<bool> CanConnectToDatabaseAsync()
        {
            bool retval = false;

            var connString = _configuration.GetConnectionString("Sql");

            using (var connection = new SqlConnection(connString))
            {
                await connection.OpenAsync();
                retval = true;
                connection.Close();
            }

            return retval;
        }

        public void Dispose()
        {
            if (_conn != null)
            {
                if (_conn.State != ConnectionState.Closed)
                {
                    _conn.Close();
                }
                _conn = null;
            }
        }

        internal async Task<SqlCommand> GetCommandAsync(string source, MigrateSourceType sourceType, int? maxRecords = null)
        {
            var sqlText = source;
            if (sourceType == MigrateSourceType.TableName)
            {
                sqlText = GetSqlStatementForTable(source, maxRecords);
            }
            var command = new SqlCommand(sqlText, await GetConnectionAsync());
            return command;
        }

        internal string GetSqlStatementForTable(string tablename, int? maxRecords = null)
        {
            var sb = new StringBuilder();
            string prefix = string.Empty;
            sb.Append($"SELECT");
            if (maxRecords != null)
            {
                sb.Append($" TOP({maxRecords})");
            }
            sb.Append($" * FROM {tablename}; ");
            return sb.ToString();
        }

        private async Task<SqlConnection> GetConnectionAsync()
        {
            if (_conn == null)
            {
                _conn = new SqlConnection(_configuration.GetConnectionString("Sql"));
            }

            switch (_conn.State)
            {
                case ConnectionState.Broken:
                case ConnectionState.Closed:
                    await _conn.OpenAsync();
                    break;

                case ConnectionState.Connecting:
                case ConnectionState.Executing:
                case ConnectionState.Fetching:
                case ConnectionState.Open:
                default:
                    break;
            }
            return _conn;
        }
    }
}