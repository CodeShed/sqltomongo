﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BCL.SqlToMongo.Repository
{
    public class MongoRepository
    {
        public MongoRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("Mongo");
        }

        private MongoClient _client;
        private IConfiguration _configuration;
        private string _connectionString;
        private IMongoDatabase _db;

        internal MongoClient Client
        {
            get
            {
                if (_client == null)
                {
                    _client = new MongoClient(_connectionString);
                }

                return _client;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="databaseName">  </param>
        /// <param name="collectionName"></param>
        /// <param name="filterJson">    "{ FirstName: 'Peter'}"</param>
        /// <returns></returns>
        public async Task<BsonDocument> FindSingleAsync(string databaseName, string collectionName, string filterJson)
        {
            var collection = GetDatabase(databaseName).GetCollection<BsonDocument>(collectionName);

            return await collection.Find(filterJson).SingleOrDefaultAsync();
        }

        internal async Task DeleteCollection(string databaseName, string collectionName)
        {
            await GetDatabase(databaseName).DropCollectionAsync(collectionName);
        }

        internal IMongoCollection<BsonDocument> GetCollection(string databaseName, string collectionName)
        {
            return GetDatabase(databaseName).GetCollection<BsonDocument>(collectionName);
        }

        internal IMongoDatabase GetDatabase(string databaseName)
        {
            if (_db == null || !_db.DatabaseNamespace.DatabaseName.Equals(databaseName, StringComparison.InvariantCultureIgnoreCase))
            {
                _db = Client.GetDatabase(databaseName);
            }
            return _db;
        }

        internal async Task<IAsyncCursor<BsonDocument>> ListCollectionsAsync(string destinationDb)
        {
            return await GetDatabase(destinationDb).ListCollectionsAsync();
        }

        internal async Task<IAsyncCursor<string>> ListDatabasesAsync()
        {
            return await Client.ListDatabaseNamesAsync();
        }

        internal async Task WriteDocumentAsync(string databaseName, string collectionName, BsonDocument doc)
        {
            await GetCollection(databaseName, collectionName).InsertOneAsync(doc);
        }

        internal async Task WriteDocumentsAsync(string databaseName, string collectionName, IEnumerable<BsonDocument> docs)
        {
            await GetCollection(databaseName, collectionName).InsertManyAsync(docs);
        }

        internal async Task WriteDocumentsAsync(string databaseName, string collectionName, List<BsonDocument> docs)
        {
            await GetCollection(databaseName, collectionName).InsertManyAsync(docs);
        }
    }
}