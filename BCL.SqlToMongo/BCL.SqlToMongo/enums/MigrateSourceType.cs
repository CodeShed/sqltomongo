﻿namespace BCL.SqlToMongo.enums
{
    public enum MigrateSourceType
    {
        TableName,
        SQLSelectStatement
    }
}