﻿using System.Threading.Tasks;
using BCL.SqlToMongo.enums;

namespace BCL.SqlToMongo
{
    public interface IMigrate
    {
        Task MigrateSqltoMongoAsync(string source, MigrateSourceType sourceType, string destinationDb, string destinationCollection, int? maxRecords = null);
    }
}