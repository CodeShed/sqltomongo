﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BCL.SqlToMongo.enums;
using BCL.SqlToMongo.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace BCL.SqlToMongo
{
    public class Migrate : IMigrate
    {
        public Migrate(IConfiguration configuration, ILogger<Migrate> logger)
        {
            _configuration = configuration;
            _maxBatchSize = Convert.ToInt32(_configuration.GetSection("SqlToMongo")["WriteToMongoBatchSize"]);
            _logger = logger;
            _logger.LogTrace($"Construct {nameof(Migrate)} completed.");
        }

        private readonly IConfiguration _configuration;
        private readonly ILogger<Migrate> _logger;
        private readonly int _maxBatchSize;

        public async Task MigrateSqltoMongoAsync(string source, MigrateSourceType sourceType, string destinationDb, string destinationCollection, int? maxRecords = null)

        {
            _logger.LogTrace($"Enter {nameof(MigrateSqltoMongoAsync)} - {nameof(source)} = {source} - {nameof(maxRecords)} = {maxRecords}.");
            int recordCount = -1;
            var sqlRepo = new SQLRepository(_configuration);
            var mongoRepo = new MongoRepository(_configuration);

            SqlDataReader reader = null;
            using (var command = await sqlRepo.GetCommandAsync(source, sourceType, maxRecords))
            {
                try
                {
                    reader = await command.ExecuteReaderAsync();
                    if (reader.HasRows)
                    {
                        var docs = new List<BsonDocument>();
                        int batchCount = 0;
                        recordCount = 0;
                        List<string> cols = null;
                        while (reader.Read())
                        {
                            #region Build ColumnNames

                            if (cols == null)
                            {
                                cols = new List<string>();
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    cols.Add(reader.GetName(i));
                                }
                            }

                            #endregion Build ColumnNames

                            var json = reader[0].ToString();
                            var row = SerializeRow(cols, reader);
                            var jsonDoc = JsonConvert.SerializeObject(row, Formatting.Indented);

                            BsonDocument BSONDoc = BsonDocument.Parse(jsonDoc);
                            docs.Add(BSONDoc);
                            batchCount++;
                            recordCount++;

                            if (batchCount > _maxBatchSize)
                            {
                                await mongoRepo.WriteDocumentsAsync(destinationDb, destinationCollection, docs);
                                batchCount = 0;
                                docs.Clear();
                            }
                        }

                        if (docs.Any())
                        {
                            await mongoRepo.WriteDocumentsAsync(destinationDb, destinationCollection, docs);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message, source, maxRecords, recordCount, _maxBatchSize);
                    throw;
                }
                finally
                {
                    reader?.Close();
                }
            }
            _logger.LogTrace($"Exit {nameof(MigrateSqltoMongoAsync)} - {nameof(source)} = {source} - {nameof(maxRecords)} = {maxRecords}.");
        }

        private Dictionary<string, object> SerializeRow(IEnumerable<string> cols,
                                                        SqlDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                result.Add(col, reader[col]);
            }

            return result;
        }
    }
}