﻿Feature: MongoRepo
	Test Writing to the Mongo Database
Background: 
	Given I connect to the Mongo Repository
	And The mongo Db has been deleted

Scenario: List Databases
	Given I connect to the Mongo Repository
	When I get a list of Mongo Databases.
	Then A list of Mongo Database names is returned.

Scenario: Add Batch of Bson Documents to a new collection
	Given I connect to the Mongo Repository	
	And the collection 'SpecialDeals' does not exist.
	When I write the folowing documents to the collection 'SpecialDeals'
	| ClassId | firstname | lastname | jss   | age |
	| 1       | Charles   | Babbage  | JSS 3 | 23  |
	| 2       | Ada       | Lovelace | JSS 4 | 24  |
	| 3       | Grace     | Hopper   | JSS 3 | 25  |

	Then the contents of the collection 'SpecialDeals' are
	| ClassId | firstname | lastname | jss   | age |
	| 1       | Charles   | Babbage  | JSS 3 | 23  |
	| 2       | Ada       | Lovelace | JSS 4 | 24  |
	| 3       | Grace     | Hopper   | JSS 3 | 25  |

Scenario: Add Bson Documents to a collection several times
	Given I connect to the Mongo Repository	
	And the collection 'SpecialDeals' does not exist.
	When I write the folowing documents to the collection 'SpecialDeals'
	| ClassId | firstname | lastname | jss   | age |
	| 1       | Charles   | Babbage  | JSS 3 | 23  |

	Then the contents of the collection 'SpecialDeals' are
	| ClassId | firstname | lastname | jss   | age |
	| 1       | Charles   | Babbage  | JSS 3 | 23  |
	
	When I write the folowing documents to the collection 'SpecialDeals'
	| ClassId | firstname | lastname | jss   | age |
	| 2       | Ada       | Lovelace | JSS 4 | 24  |
	
	And I write the folowing documents to the collection 'SpecialDeals'
	| ClassId | firstname | lastname | jss   | age |
	| 3       | Grace     | Hopper   | JSS 3 | 25  |
	| 444     | Marget    | Folwer   | Hut 3 | 44  |

	Then the contents of the collection 'SpecialDeals' are
	| ClassId | firstname | lastname | jss   | age |
	| 1       | Charles   | Babbage  | JSS 3 | 23  |
	| 2       | Ada       | Lovelace | JSS 4 | 24  |
	| 3       | Grace     | Hopper   | JSS 3 | 25  |
	| 444     | Marget    | Folwer   | Hut 3 | 44  |
