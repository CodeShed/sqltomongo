﻿Feature: Migration
	I want to test actual migration 

Background: 
	Given The mongo Db has been deleted
	
Scenario: WriteTableToMongo
	Given I connect to the Mongo Repository	
	And the collection 'PaymentMethods' does not exist.
	When I Migrate the table 'Application.PaymentMethods' into collection 'PaymentMethods'
	Then the contents of the collection 'PaymentMethods' are
	| PaymentMethodID | PaymentMethodName | LastEditedBy | ValidFrom                   | ValidTo                     |
	| 1               | Cash              | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	| 2               | Check             | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	| 3               | Credit-Card       | 9            | 2016-01-01 16:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	| 4               | EFT               | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |

Scenario: WriteTableToMongoUsingSQL
	Given I connect to the Mongo Repository	
	And the collection 'PaymentMethods' does not exist.
	When I Migrate the following SQL statement into collection 'PaymentMethods'
	| SQL                                                 |
	| Select * from [Application].[PaymentMethods]; |
	Then the contents of the collection 'PaymentMethods' are
	| PaymentMethodID | PaymentMethodName | LastEditedBy | ValidFrom                   | ValidTo                     |
	| 1               | Cash              | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	| 2               | Check             | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	| 3               | Credit-Card       | 9            | 2016-01-01 16:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	| 4               | EFT               | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	
Scenario: WriteFirst3PaymentMethodsToMongo
	Given I connect to the Mongo Repository	
	And the collection 'PaymentMethods' does not exist.
	When I Migrate the first '3' records of table 'Application.PaymentMethods' into collection 'PaymentMethods'
	Then the contents of the collection 'PaymentMethods' are
	| PaymentMethodID | PaymentMethodName | LastEditedBy | ValidFrom                   | ValidTo                     |
	| 1               | Cash              | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	| 2               | Check             | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	| 3               | Credit-Card       | 9            | 2016-01-01 16:00:00.0000000 | 9999-12-31 23:59:59.9999999 |

Scenario: WriteFirst3PaymentMethodsToMongoUsingSQL
	Given I connect to the Mongo Repository	
	And the collection 'PaymentMethods' does not exist.
	When I Migrate the following SQL statement into collection 'PaymentMethods'
	| SQL                                                 |
	| Select Top 2 * from [Application].[PaymentMethods]; |
	Then the contents of the collection 'PaymentMethods' are
	| PaymentMethodID | PaymentMethodName | LastEditedBy | ValidFrom                   | ValidTo                     |
	| 1               | Cash              | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
	| 2               | Check             | 1            | 2013-01-01 00:00:00.0000000 | 9999-12-31 23:59:59.9999999 |
