﻿Feature: SqlRepo
	Check the Functionality of the SqlRepository

@mytag
Scenario: Connect to the database
	When I try to connect to the SQL database
	Then Result as boolean is 'true'

Scenario: I can generate Sql Statement for table '[Sales].[Invoices]'
	Given I can connect to the SQL database
	When I generate Sql Statement for table '[Sales].[Invoices]'
	Then Result as string is 'SELECT * FROM [Sales].[Invoices];'

Scenario: I can generate Sql Statement for table '[Purchasing].[PurchaseOrders]'
	Given I can connect to the SQL database
	When I generate Sql Statement for table '[Purchasing].[PurchaseOrders]'
	Then Result as string is 'SELECT * FROM [Purchasing].[PurchaseOrders];'

Scenario: I can generate Sql Statement for table 'Warehouse.VehicleTemperatures'
	Given I can connect to the SQL database
	When I generate Sql Statement for table 'Warehouse.VehicleTemperatures'
	Then Result as string is 'SELECT * FROM Warehouse.VehicleTemperatures;'

