﻿using System;

namespace BCL.SqlToMongo.SpecFlow.Models
{
    public class PaymentMethods
    {
        public int PaymentMethodID { get; set; }
        public string PaymentMethodName { get; set; }
        public int LastEditedBy { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
    }
}