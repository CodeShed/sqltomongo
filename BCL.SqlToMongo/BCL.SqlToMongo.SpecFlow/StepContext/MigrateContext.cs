﻿namespace BCL.SqlToMongo.SpecFlow.StepContext
{
    public class MigrateContext : BaseContext
    {
        public IMigrate Migrate { get; set; }
    }
}