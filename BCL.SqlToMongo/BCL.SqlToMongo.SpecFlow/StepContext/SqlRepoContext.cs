﻿using BCL.SqlToMongo.Repository;

namespace BCL.SqlToMongo.SpecFlow.StepContext
{
    public class SqlRepoContext : BaseContext
    {
        public bool? CanConnectToDatabase { get; set; }
        public SQLRepository Repo { get; set; }
    }
}