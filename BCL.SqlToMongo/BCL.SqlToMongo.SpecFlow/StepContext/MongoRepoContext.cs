﻿using System.Collections.Generic;
using BCL.SqlToMongo.Repository;
using MongoDB.Bson;

namespace BCL.SqlToMongo.SpecFlow.StepContext
{
    public class MongoRepoContext : BaseContext
    {
        private MongoRepository _repo;

        public IEnumerable<BsonDocument> ActualCollection { get; set; }
        public string CollectionName { get; set; }

        public IEnumerable<BsonDocument> ExpectedCollection { get; internal set; }
        public IEnumerable<BsonDocument> ExpectedDocuments { get; set; }

        public MongoRepository Repo
        {
            get => _repo;
            set
            {
                _repo = value;
                Results = null;
            }
        }
    }
}