﻿namespace BCL.SqlToMongo.SpecFlow.StepContext
{
    public abstract class BaseContext : IContext
    {
        public BaseContext()
        {
            MongoDbName = "BCLSQLToMongoSpecFlowTests";
        }

        public dynamic Results { get; set; }
        public string MongoDbName { get; set; }
    }
}