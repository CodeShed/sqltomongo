﻿namespace BCL.SqlToMongo.SpecFlow.StepContext
{
    public interface IContext
    {
        dynamic Results { get; set; }
        string MongoDbName { get; set; }
    }
}