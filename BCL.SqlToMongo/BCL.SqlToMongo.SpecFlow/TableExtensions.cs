﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace BCL.SqlToMongo.Specflow
{
    public static class TableExtensions
    {
        public static void CompareToBsonDocumentSet<T>(this Table table, IEnumerable<BsonDocument> bsonDocumentList)
        {
            var target = new List<T>();
            foreach (var item in bsonDocumentList)
            {
                item.Remove("_id");
                var x = BsonSerializer.Deserialize<T>(item);
                target.Add(x);
            }
            table.CompareToSet<T>(target);
        }

        public static IEnumerable<BsonDocument> ToBsonDocumentList(this Table table)
        {
            List<BsonDocument> docs = null;
            foreach (TableRow row in table.Rows)
            {
                var doc = row.ToBsonDocument();

                if (docs == null)
                {
                    docs = new List<BsonDocument>();
                }

                docs.Add(doc);
            }
            return docs;
        }

        public static string GetFirstCell(this Table table, string colName)
        {
            var retval = table.Rows[0].GetString(colName);
            return retval;
        }
    }
}