﻿using System.Threading.Tasks;
using BCL.SqlToMongo.Repository;
using BCL.SqlToMongo.SpecFlow.StepContext;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace BCL.SqlToMongo.Specflow.Steps
{
    [Binding]
    public class SqlRepoSteps
    {
        public SqlRepoSteps(SqlRepoContext context)
        {
            _context = context;
        }

        private readonly SqlRepoContext _context;

        [Given(@"I can connect to the SQL database")]
        public void GivenICanConnectToTheDatabase()
        {
            _context.Repo = new SQLRepository(TestHelper.GetConfigurationRoot());
        }

        [Then(@"Result as string is '(.*)'")]
        public void ThenResultAsStringIs(string expected)
        {
            _context.Results.ToString().Equals(expected);
        }

        [Then(@"Result as boolean is '(.*)'")]
        public void ThenResultIs(string expectedResult)
        {
            var expected = bool.Parse(expectedResult);
            _context.CanConnectToDatabase.Should().Be(expected);
        }

        [When(@"I try to connect to the SQL database")]
        public async Task WhenIConnectToTheDatabaseServerAsync()
        {
            _context.Repo = new SQLRepository(TestHelper.GetConfigurationRoot());
            _context.CanConnectToDatabase = await _context.Repo.CanConnectToDatabaseAsync();
        }

        [When(@"I generate Sql Statement for table '(.*)'")]
        public void WhenIGenerateSqlStatementForTable(string sourceTable)
        {
            _context.Results = _context.Repo.GetSqlStatementForTable(sourceTable);
        }
    }
}