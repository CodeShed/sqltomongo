﻿using System.Threading.Tasks;
using BCL.SqlToMongo.Repository;
using BCL.SqlToMongo.SpecFlow.Models;
using BCL.SqlToMongo.SpecFlow.StepContext;
using FluentAssertions;
using MongoDB.Driver;
using TechTalk.SpecFlow;

namespace BCL.SqlToMongo.Specflow.Steps
{
    [Binding]
    public class MongoRepoSteps
    {
        public MongoRepoSteps(MongoRepoContext context)
        {
            _context = context;
        }

        private readonly MongoRepoContext _context;

        [Given(@"I connect to the Mongo Repository")]
        public void GivenICreateTheMongoRepository()
        {
            _context.Repo = new MongoRepository(TestHelper.GetConfigurationRoot());
        }

        [Given(@"the collection '(.*)' does not exist\.")]
        public async Task GivenTheCollectionDoesNotExistAsync(string colectionName)
        {
            await _context.Repo.DeleteCollection(_context.MongoDbName, colectionName);
        }

        [Given(@"The mongo Db has been deleted")]
        public void GivenTheMongoDbHasBeenDeleted()
        {
            var sut = new MongoRepository(TestHelper.GetConfigurationRoot());
            sut.Client.DropDatabase(_context.MongoDbName);
        }

        [Then(@"A list of Mongo Database names is returned\.")]
        public async Task ThenAListOfMongoDatabaseNamesIsReturned()
        {
            var results = await ((IAsyncCursor<string>)_context.Results).AnyAsync();

            results.Should().BeTrue();
        }

        [Then(@"the contents of the collection '(.*)' are")]
        public async Task ThenTheContentsOfTheCollectionAreAsync(string collectionName, Table table)
        {
            var actual = await _context.Repo.GetCollection(_context.MongoDbName, collectionName).Find(f => true).ToListAsync();
            if (collectionName.Equals("SpecialDeals"))
            {
                table.CompareToBsonDocumentSet<SpecialDeals>(actual);
            }
            else if (collectionName.Equals("PaymentMethods"))
            {
                table.CompareToBsonDocumentSet<PaymentMethods>(actual);
            }
        }

        [When(@"I get a list of Mongo Databases\.")]
        public async Task WhenIGetAListOfMongoDatabasesAsync()
        {
            _context.Results = await _context.Repo.ListDatabasesAsync();
        }

        [When(@"I write the folowing documents to the collection '(.*)'")]
        public async Task WhenIWriteTheDocumentsToTheCollectionAsync(string collectionName, Table table)
        {
            await _context.Repo.WriteDocumentsAsync(_context.MongoDbName, collectionName, table.ToBsonDocumentList());
        }
    }
}