﻿using System.Threading.Tasks;
using BCL.SqlToMongo.SpecFlow.StepContext;
using TechTalk.SpecFlow;

namespace BCL.SqlToMongo.Specflow.Steps
{
    [Binding]
    public class MigrationSteps
    {
        private readonly MigrateContext _context;

        public MigrationSteps(MigrateContext migrateContext)
        {
            _context = migrateContext;
        }

        [When(@"I Migrate the first '(.*)' records of table '(.*)' into collection '(.*)'")]
        public async Task WhenIMigrateTheFirstRecordsOfTableIntoCollectionAsync(int recordsToMigrate,
                                                                                string sourceTable,
                                                                                string destinationCollection)
        {
            _context.Migrate = new Migrate(TestHelper.GetConfigurationRoot(), TestHelper.GetILoggerFactory<Migrate>());
            await _context.Migrate.MigrateSqltoMongoAsync(sourceTable,
                                                        enums.MigrateSourceType.TableName,
                                                        _context.MongoDbName,
                                                        destinationCollection,
                                                        recordsToMigrate);
        }

        [When(@"I Migrate the table '(.*)' into collection '(.*)'")]
        public async Task WhenIMigrateTheTableIntoCollection(string sourceTable, string destinationCollection)
        {
            _context.Migrate = new Migrate(TestHelper.GetConfigurationRoot(), TestHelper.GetILoggerFactory<Migrate>());
            await _context.Migrate.MigrateSqltoMongoAsync(sourceTable,
                                                        enums.MigrateSourceType.TableName,
                                                        _context.MongoDbName,
                                                        destinationCollection);
        }

        [When(@"I Migrate the following SQL statement into collection '(.*)'")]
        public async Task WhenIMigrateTheFollowingSQLStatementIntoCollection(string destinationCollection, Table table)
        {
            _context.Migrate = new Migrate(TestHelper.GetConfigurationRoot(), TestHelper.GetILoggerFactory<Migrate>());
            await _context.Migrate.MigrateSqltoMongoAsync(table.GetFirstCell("SQL"),
                                                        enums.MigrateSourceType.SQLSelectStatement,
                                                        _context.MongoDbName,
                                                        destinationCollection);
        }
    }
}