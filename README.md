# SqlToMongo

Open-Source tool to migrate MS-SQL database table to MongoDb using .net Standard 2.1

The migration will work in one of several ways:- 
* Specifing the source and the destination then the code will migrate the complete table accross.
* By supplying an T-SQL query to retrieve the data.

Your project will need a reference to BCL.SqlToMongo

### To Migrate a complete table use the following code.
```
    await _context.Migrate.MigrateSqltoMongoAsync(sourceTable,
                                                  MigrateSourceType.TableName,
                                                  MongoDbName,
                                                  destinationCollectionName);
```
    

### To Migrate using a SQL statement use  the following.
```
    await _context.Migrate.MigrateSqltoMongoAsync(SQLQuerry,
                                                  MigrateSourceType.SQLSelectStatement,
                                                  MongoDbName,
                                                  destinationCollectionName);
```
